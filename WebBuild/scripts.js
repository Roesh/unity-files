/* This javascript file is used to control the arm. 
TODO: understand how the file should be saved so that Unity can access it (in Plugins folder, .jslib extension)
TODO: Add thumb functionality
TODO: Create a function that initializes final angles to some default values other than 0
TODO: Create sendMessage functionality*/

var manualMode = true;
var num_joints = 4;	//Obtain this either from the webpage or the unity script 
var dbflag = true;
// Joint variables
var runJointFlags 	= new Array(num_joints);
var total_times 	= new Array(num_joints);
var total_degrees 	= new Array(num_joints);
var axis_chars 		= new Array(num_joints);

// Initialize the arrays
resetAllVariables();

function modeChange(){
	manualMode = !manualMode;
}
function recreateArrays(){
	runJointFlags 	= new Array(num_joints);
	total_times 	= new Array(num_joints);
	total_degrees 	= new Array(num_joints);
	axis_chars 		= new Array(num_joints);
	resetAllVariables();
}
// The problem may actually lie in the inability of the SendMessage function to work.
// If reimplementing, use the gameInstance.SendMessage and see if that works
function testSet(){
	for(var index = 0; index < num_joints; index++){
		axis_chars[index] 	 = 'x';
		total_times[index]	 = 3.0;
		total_degrees[index] = 60.0;
		runJointFlags[index] =   true;
	}
}	
// Selectively flag a joint to run
function setData(){
	// This will be replaced by data input from webpage
	var ID; var ax; var t; var an;
	ID = document.getElementByID("ID").value;
	t = document.getElementByID('time').value;
	ax = document.getElementByID('axis').value;
	an = document.getElementByID('angle').value;

	var index = ID - 1;
	runJointFlags[index] = 1;
	axis_chars[index] 	 = ax;
	total_times[index]	 = t;
	total_degrees[index] = an;
}
// Reset
function resetAllVariables(){ 
	for(var i = 0; i < num_joints; i++){
		runJointFlags[i] 	=    0;
		total_degrees[i]	=  0.0;
		total_times[i] 		=  0.0;
		axis_chars[i]		=  'x';
	}
}
function Debug(){
	var k = "la";
	for(var i = 0; i < num_joints; i++){
		k += " ";
		var n = total_degrees[i];
		k+= n.toString();
	}
	alert(k);
}
function debug(input){
		//alert(input);	
}

